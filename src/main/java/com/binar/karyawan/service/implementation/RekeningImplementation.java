package com.binar.karyawan.service.implementation;

import com.binar.karyawan.entity.Karyawan;
import com.binar.karyawan.entity.Rekening;
import com.binar.karyawan.helper.ResponseHelper;
import com.binar.karyawan.repository.KaryawanRepo;
import com.binar.karyawan.repository.RekeningRepo;
import com.binar.karyawan.service.interfaces.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RekeningImplementation implements RekeningService {
    @Autowired
    RekeningRepo rekeningRepo;

    @Autowired
    KaryawanRepo karyawanRepo;

    ResponseHelper respHelper;

    @Override
    public Map insert(Rekening rekening, Long idKaryawan) {
        Karyawan objKaryawan = karyawanRepo.getById(idKaryawan);
        rekening.setKaryawan(objKaryawan);

        try {
            Rekening obj = rekeningRepo.save(rekening);
            return respHelper.success(obj, "Data rekening sukses dibuat");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map update(Rekening rekening) {
        try {
            Rekening obj = rekeningRepo.getById(rekening.getId());

            if (obj == null) {
                return respHelper.notFound();
            }

            obj.setNama(rekening.getNama());
            obj.setJenis(rekening.getJenis());
            obj.setUpdated_at(new Date());
            obj.setNomor(rekening.getNomor());
            rekeningRepo.save(obj);
            return respHelper.success(obj, "Data rekening sukses diperbarui");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map delete(Long idRekening) {
        try {
            Rekening obj = rekeningRepo.getById(idRekening);

            if (obj == null) {
                return respHelper.notFound();
            }

            obj.setDeleted_at(new Date());
            rekeningRepo.save(obj);
            return respHelper.success(obj, "Data rekening sukses dihapus");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map getByKaryawanId(Long idKaryawan) {
        List<Rekening> obj = new ArrayList<Rekening>();
        try {
            obj = rekeningRepo.getByKaryawanId(idKaryawan);

            if (obj == null) {
                return respHelper.notFound();
            }

            return respHelper.success(obj, "Sukses menampilkan data rekening");

        } catch (Exception e) {
            e.printStackTrace();
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map getByJenisAndKaryawanId(String jenis, Long idKaryawan) {
        List<Rekening> obj = new ArrayList<Rekening>();
        try {
            obj = rekeningRepo.getByKaryawanId(idKaryawan);

            if (obj == null) {
                return respHelper.notFound();
            }

            return respHelper.success(obj, "Sukses menampilkan data rekening");

        } catch (Exception e) {
            e.printStackTrace();
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map getById(Long idRekening) {
        try {
            Rekening obj = rekeningRepo.getById(idRekening);

            if (obj == null) {
                return respHelper.notFound();
            }

            return respHelper.success(obj, "Sukses menampilkan data rekening");

        } catch (Exception e) {
            e.printStackTrace();
            return respHelper.internalServerError(e);
        }
    }
}
