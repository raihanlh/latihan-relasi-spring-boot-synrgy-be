package com.binar.karyawan.service.implementation;

import com.binar.karyawan.entity.Karyawan;
import com.binar.karyawan.entity.KaryawanTraining;
import com.binar.karyawan.entity.Training;
import com.binar.karyawan.helper.ResponseHelper;
import com.binar.karyawan.repository.KaryawanRepo;
import com.binar.karyawan.repository.KaryawanTrainingRepo;
import com.binar.karyawan.repository.TrainingRepo;
import com.binar.karyawan.service.interfaces.KaryawanTrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Map;

@Service
@Transactional
public class KaryawanTrainingImplementation implements KaryawanTrainingService {
    @Autowired
    KaryawanTrainingRepo karyawanTrainingRepo;

    @Autowired
    KaryawanRepo karyawanRepo;

    @Autowired
    TrainingRepo trainingRepo;

    ResponseHelper respHelper;

    @Override
    public Map insert(KaryawanTraining karyawanTraining) {
        try {
            Karyawan objKaryawan = karyawanRepo.getById(karyawanTraining.getKaryawan().getId());
            if (objKaryawan == null) {
                return respHelper.notFound("Karyawan not found");
            }

            Training objTraining = trainingRepo.getById(karyawanTraining.getTraining().getId());
            if (objTraining == null) {
                return respHelper.notFound("Training not found");
            }

            karyawanTraining.setKaryawan(objKaryawan);
            karyawanTraining.setTraining(objTraining);

            KaryawanTraining resp = karyawanTrainingRepo.save(karyawanTraining);
            return respHelper.success(resp, "Sukses memasukkan data training karyawan");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map update(KaryawanTraining karyawanTraining) {
        try {
            KaryawanTraining obj = karyawanTrainingRepo.getById(karyawanTraining.getId());
            Karyawan karyawan = karyawanRepo.getById(karyawanTraining.getKaryawan().getId());
            Training training = trainingRepo.getById(karyawanTraining.getTraining().getId());

            if (karyawanTraining.getTanggal() != null) {
                obj.setTanggal(karyawanTraining.getTanggal());
            }
            obj.setKaryawan(karyawan);
            obj.setTraining(training);

            KaryawanTraining result = karyawanTrainingRepo.save(obj);

            return respHelper.success(result, "Sukses");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map delete(Long idKaryawanTraining) {
        try {
            KaryawanTraining obj = karyawanTrainingRepo.getById(idKaryawanTraining);

            obj.setDeleted_at(new Date());

            KaryawanTraining result = karyawanTrainingRepo.save(obj);
            return respHelper.success(result, "Sukses");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map getById(Long idKaryawanTraining) {
        return null;
    }

    @Override
    public Map getAll(Integer page, Integer size, String sortAttribute, String sortType) {
        Page<KaryawanTraining> dataRes;
        Pageable options;
        sortAttribute = sortAttribute.equals("") ? "id" : sortAttribute;
        try {
            if ((sortType == "desc") || (sortType == "descending")) {
                options = PageRequest.of(page, size, Sort.by(sortAttribute).descending());
            } else {
                options = PageRequest.of(page, size, Sort.by(sortAttribute).ascending());
            }
            dataRes = karyawanTrainingRepo.findAll(options);

            return respHelper.success(dataRes.getContent(), "Sukses menampilkan semua data training karyawan");

        } catch (Exception e) {
            e.printStackTrace();
            return respHelper.internalServerError(e);
        }
    }
}
