package com.binar.karyawan.service.implementation;

import com.binar.karyawan.entity.Training;
import com.binar.karyawan.helper.ResponseHelper;
import com.binar.karyawan.repository.TrainingRepo;
import com.binar.karyawan.service.interfaces.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Map;

@Service
@Transactional
public class TrainingImplementation implements TrainingService {

    @Autowired
    TrainingRepo trainingRepo;

    ResponseHelper respHelper;

    @Override
    public Map insert(Training training) {
        try {
            Training objTraining = trainingRepo.save(training);

            return respHelper.success(objTraining, "Berhasil membuat training baru");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map update(Training training) {
        try {
            Training objTraining = trainingRepo.getById(training.getId());

            objTraining.setNamapengajar(training.getNamapengajar());
            objTraining.setUpdated_at(new Date());
            objTraining.setTema(training.getTema());

            trainingRepo.save(objTraining);
            return respHelper.success(objTraining, "Berhasil mengubah training");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map delete(long idTraining) {
        try {
            Training obj = trainingRepo.getById(idTraining);

            if (obj == null) {
                return respHelper.notFound();
            }

            obj.setDeleted_at(new Date());
            trainingRepo.save(obj);
            return respHelper.success(obj, "Data karyawan sukses dihapus");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map getById(Long idTraining) {
        try {
            Training obj = trainingRepo.getById(idTraining);

            if (obj == null) {
                return respHelper.notFound();
            }

            return respHelper.success(obj, "Sukses menampilkan data training");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map getAll(Integer page, Integer size, String sortAttribute, String sortType) {
        try {
            Page<Training> dataRes;
            Pageable options;
            sortAttribute = sortAttribute.equals("") ? "id" : sortAttribute;
            try {
                if ((sortType == "desc") || (sortType == "descending")) {
                    options = PageRequest.of(page, size, Sort.by(sortAttribute).descending());
                } else {
                    options = PageRequest.of(page, size, Sort.by(sortAttribute).ascending());
                }
                dataRes = trainingRepo.findAll(options);

                return respHelper.success(dataRes.getContent(), "Sukses menampilkan semua data training");

            } catch (Exception e) {
                e.printStackTrace();
                return respHelper.internalServerError(e);
            }
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }
}
