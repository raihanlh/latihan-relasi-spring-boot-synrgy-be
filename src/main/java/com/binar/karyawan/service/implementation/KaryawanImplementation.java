package com.binar.karyawan.service.implementation;

import com.binar.karyawan.entity.DetailKaryawan;
import com.binar.karyawan.entity.Karyawan;
import com.binar.karyawan.helper.ResponseHelper;
import com.binar.karyawan.repository.DetailKaryawanRepo;
import com.binar.karyawan.repository.KaryawanRepo;
import com.binar.karyawan.service.interfaces.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class KaryawanImplementation implements KaryawanService {
    @Autowired
    KaryawanRepo karyawanRepo;

    @Autowired
    DetailKaryawanRepo detailKaryawanRepo;

    ResponseHelper respHelper;

    @Override
    public Map insert(Karyawan karyawan) {
        try {
            DetailKaryawan detailKaryawan = null;

            if (karyawan.getDetailkaryawan() != null) {
                DetailKaryawan objDetailKaryawan = karyawan.getDetailkaryawan();
                detailKaryawan = detailKaryawanRepo.save(objDetailKaryawan);
            }
            Karyawan obj = karyawanRepo.save(karyawan);
            detailKaryawan.setKaryawan(obj);
            detailKaryawanRepo.save(detailKaryawan);
            return respHelper.success(obj, "Data karyawan sukses dibuat");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map update(Karyawan karyawan) {
        try {
            Karyawan obj = karyawanRepo.getById(karyawan.getId());
            DetailKaryawan detailKaryawan = null;

            if (obj == null) {
                return respHelper.notFound();
            }

            if (karyawan.getDetailkaryawan() != null) {
                detailKaryawan = detailKaryawanRepo.getById(obj.getDetailkaryawan().getId());

                if (detailKaryawan != null) {
                    detailKaryawan.setNik(karyawan.getDetailkaryawan().getNik());
                    detailKaryawan.setNpwp(karyawan.getDetailkaryawan().getNpwp());
                    DetailKaryawan newDetailKaryawan = detailKaryawanRepo.save(detailKaryawan);
                    obj.setDetailkaryawan(newDetailKaryawan);
                }
                else {
                    DetailKaryawan objDetailKaryawan = karyawan.getDetailkaryawan();
                    objDetailKaryawan.setKaryawan(obj);
                    objDetailKaryawan.setId(karyawan.getDetailkaryawan().getId());
                    detailKaryawan = detailKaryawanRepo.save(objDetailKaryawan);
                    obj.setDetailkaryawan(detailKaryawan);
                }

            }

            obj.setNama(karyawan.getNama());
            obj.setAlamat(karyawan.getAlamat());
            obj.setTanggallahir(karyawan.getTanggallahir());
            obj.setStatus(karyawan.getStatus());
            obj.setUpdated_at(new Date());
            obj.setFilename(karyawan.getFilename());
            Karyawan result = karyawanRepo.save(obj);
            return respHelper.success(result, "Data karyawan sukses diperbarui");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map getAll(Integer page, Integer size, String sortAttribute, String sortType) {
        // List<Karyawan> list = new ArrayList<Karyawan>();
        Page<Karyawan> dataRes;
        Pageable options;
        sortAttribute = sortAttribute.equals("") ? "id" : sortAttribute;
        try {
            if ((sortType == "desc") || (sortType == "descending")) {
                options = PageRequest.of(page, size, Sort.by(sortAttribute).descending());
            } else {
                options = PageRequest.of(page, size, Sort.by(sortAttribute).ascending());
            }
            dataRes = karyawanRepo.findAll(options);

            return respHelper.success(dataRes.getContent(), "Sukses menampilkan semua data karyawan");

        } catch (Exception e) {
            e.printStackTrace();
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map getById(Long idKaryawan) {
        try {
            Karyawan obj = karyawanRepo.getById(idKaryawan);

            if (obj == null) {
                return respHelper.notFound();
            }

            return respHelper.success(obj, "Sukses menampilkan data karyawan");

        } catch (Exception e) {
            e.printStackTrace();
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map delete(Long idKaryawan) {
        try {
            Karyawan obj = karyawanRepo.getById(idKaryawan);

            if (obj == null) {
                return respHelper.notFound();
            }

            obj.setDeleted_at(new Date());
            obj.setStatus(2);
            karyawanRepo.save(obj);
            return respHelper.success(obj, "Data karyawan sukses dihapus");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    public List<Karyawan> listKaryawanThymeleaf(int pageNumber, int ROW_PER_PAGE) {
        List<Karyawan> karyawans = new ArrayList<>();
        Pageable sortedIdASC = PageRequest.of(pageNumber - 1, ROW_PER_PAGE, Sort.by("id").ascending());
        karyawanRepo.findAll(sortedIdASC).forEach(karyawans::add);
        return karyawans;
    }

    public Long count() {
        return karyawanRepo.count();
    }

    public Karyawan findById(Long karyawanId) {
        try {
            Karyawan obj = karyawanRepo.getById(karyawanId);

            return obj;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
