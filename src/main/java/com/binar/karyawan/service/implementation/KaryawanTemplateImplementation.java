package com.binar.karyawan.service.implementation;

import com.binar.karyawan.entity.Karyawan;
import com.binar.karyawan.helper.ResponseHelper;
import com.binar.karyawan.service.interfaces.KaryawanTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;

@Service
@Transactional
public class KaryawanTemplateImplementation implements KaryawanTemplateService {
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    private ResponseHelper responseHelper;

    @Override
    public Map insert(Karyawan karyawan) {
        try {
            String url = "http://localhost:9090/v1/karyawan/save";

            ResponseEntity<Map> result = restTemplateBuilder.build().postForEntity(url, karyawan, Map.class);

            return responseHelper.success(result.getBody(), "Sukses");
        } catch (Exception e) {
            return responseHelper.internalServerError(e);
        }
    }

    @Override
    public Map update(Karyawan karyawan) {
        try {
            String url = "http://localhost:9090/v1/karyawan/update/" + karyawan.getId();

            HttpEntity<Karyawan> req = new HttpEntity<>(karyawan);
            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(url, HttpMethod.PUT,req, new
                    ParameterizedTypeReference<Map>(){});

            return responseHelper.success(result.getBody(), "Sukses");
        } catch (Exception e) {
            return responseHelper.internalServerError(e);
        }
    }

    @Override
    public Map getAll() {
        try {
            String url = "http://localhost:9090/v1/karyawan/all";

            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(url, HttpMethod.GET,null, new
                    ParameterizedTypeReference<Map>(){});

            return responseHelper.success(result.getBody(), "Sukses");
        } catch (Exception e) {
            return responseHelper.internalServerError(e);
        }
    }

    @Override
    public Map getById(Long idKaryawan) {
        return null;
    }

    @Override
    public Map delete(Long idKaryawan) {
        try {
            String url = "http://localhost:9090/v1/karyawan/delete/" + idKaryawan;

            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(url, HttpMethod.DELETE, null, new
                    ParameterizedTypeReference<Map>(){});

            return responseHelper.success(result.getBody(), "Sukses");
        } catch (Exception e) {
            return responseHelper.internalServerError(e);
        }

    }
}
