package com.binar.karyawan.service.interfaces;

import com.binar.karyawan.entity.KaryawanTraining;
import com.binar.karyawan.entity.Training;

import java.util.Map;

public interface KaryawanTrainingService {
    public Map insert(KaryawanTraining karyawanTraining);

    public Map update(KaryawanTraining karyawanTraining);

    public Map delete(Long idKaryawanTraining);

    public Map getById(Long idKaryawanTraining);

    public Map getAll(Integer page, Integer size, String sortAttribute, String sortType);
}
