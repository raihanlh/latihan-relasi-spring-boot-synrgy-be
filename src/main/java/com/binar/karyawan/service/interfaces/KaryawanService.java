package com.binar.karyawan.service.interfaces;

import com.binar.karyawan.entity.Karyawan;

import java.util.List;
import java.util.Map;

public interface KaryawanService {
    public Map insert(Karyawan karyawan);

    public Map update(Karyawan karyawan);

    public Map getAll(Integer page, Integer size, String sortAttribute, String sortType);

    public Map getById(Long idKaryawan);

    public Map delete(Long idKaryawan);

    public List<Karyawan> listKaryawanThymeleaf(int pageNumber, int ROW_PER_PAGE);

    public Long count();

    public Karyawan findById(Long karyawanId);
}
