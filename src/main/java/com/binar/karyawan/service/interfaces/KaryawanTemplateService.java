package com.binar.karyawan.service.interfaces;

import com.binar.karyawan.entity.Karyawan;

import java.util.Map;

public interface KaryawanTemplateService {
    public Map insert(Karyawan karyawan);

    public Map update(Karyawan karyawan);

    public Map getAll();

    public Map getById(Long idKaryawan);

    public Map delete(Long idKaryawan);
}
