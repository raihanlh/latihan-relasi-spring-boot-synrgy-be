package com.binar.karyawan.service.interfaces;

import com.binar.karyawan.entity.Training;

import java.util.Map;

public interface TrainingService {
    public Map insert(Training training);

    public Map update(Training training);

    public Map delete(long idTraining);

    public Map getById(Long idTraining);

    public Map getAll(Integer page, Integer size, String sortAttribute, String sortType);
}
