package com.binar.karyawan.service.interfaces;

import com.binar.karyawan.entity.Karyawan;
import com.binar.karyawan.entity.Rekening;

import java.util.Map;

public interface RekeningService {
    public Map insert(Rekening rekening, Long idKaryawan);

    public Map update(Rekening rekening);

    public Map delete(Long idRekening);

    public Map getByKaryawanId(Long idKaryawan);

    public Map getById(Long idRekening);

    public Map getByJenisAndKaryawanId(String jenis, Long idKaryawan);
}
