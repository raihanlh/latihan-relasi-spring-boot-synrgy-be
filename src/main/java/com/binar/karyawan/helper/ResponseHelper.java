package com.binar.karyawan.helper;

import java.util.HashMap;
import java.util.Map;

public class ResponseHelper {
    public static Map success(Object data, String message) {
        Map<String, Object> map = new HashMap<>();

        map.put("data", data);
        map.put("status", 200);
        map.put("message", message);

        return map;
    }

    public static Map notFound() {
        Map<String, Object> map = new HashMap<>();

        map.put("status", 404);
        map.put("message", "Not Found");

        return map;
    }

    public static Map notFound(String message) {
        Map<String, Object> map = new HashMap<>();

        map.put("status", 404);
        map.put("message", message);

        return map;
    }

    public static Map internalServerError(Exception e) {
        Map<String, Object> map = new HashMap<>();

        map.put("status", 500);
        map.put("message", e);

        return map;
    }
}
