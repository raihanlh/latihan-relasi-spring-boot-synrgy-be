package com.binar.karyawan.controller;

import com.binar.karyawan.entity.DetailKaryawan;
import com.binar.karyawan.entity.Karyawan;
import com.binar.karyawan.repository.DetailKaryawanRepo;
import com.binar.karyawan.repository.KaryawanRepo;
import com.binar.karyawan.service.interfaces.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller // Bukan RestController
@RequestMapping("/v1/view")
public class ThymeleafController {
    @Autowired
    KaryawanService karyawanService;

    @Autowired
    KaryawanRepo karyawanRepo;

    @Autowired
    DetailKaryawanRepo detailKaryawanRepo;

    private final int ROW_PER_PAGE = 5;

    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        model.addAttribute("title", "Karyawan");
        return "index";
    }

    @GetMapping(value = "/karyawan")
    public String getKaryawan(Model model, @RequestParam(value = "page", defaultValue = "1") int pageNumber) {
        List<Karyawan> karyawanlst = karyawanService.listKaryawanThymeleaf(pageNumber, ROW_PER_PAGE);

        long count = karyawanService.count();
        boolean hasPrev = pageNumber > 1;
        boolean hasNext = (pageNumber * ROW_PER_PAGE) < count;
        model.addAttribute("karyawanlst", karyawanlst);
        model.addAttribute("hasPrev", hasPrev);
        model.addAttribute("prev", pageNumber - 1);
        model.addAttribute("hasNext", hasNext);
        model.addAttribute("next", pageNumber + 1);
        return "karyawan-list";
    }

    @GetMapping(value = {"/karyawan/add"})
    public String showAddKaryawan(Model model) {
        Karyawan karyawan = new Karyawan();
        model.addAttribute("add", true);
        model.addAttribute("karyawan", karyawan);

        return "karyawan-edit";
    }

    @GetMapping(value = "/karyawan/{karyawanId}")
    public String getKaryawanById(Model model, @PathVariable Long karyawanId) {
        Karyawan karyawan = null;
        try {
            karyawan = karyawanService.findById(karyawanId);
        } catch (Exception ex) {
            model.addAttribute("errorMessage", "Karyawan not found");
        }
        model.addAttribute("karyawan", karyawan);
        return "karyawan";
    }

    @PostMapping(value = "/karyawan/add")
    public String addKaryawan(Model model,
                             @ModelAttribute("karyawan") Karyawan karyawan) {
        try {
            DetailKaryawan detailKaryawan = karyawan.getDetailkaryawan();

            if (detailKaryawan != null) {
                detailKaryawan = detailKaryawanRepo.save(detailKaryawan);
            }
            Karyawan newKaryawan = karyawanRepo.save(karyawan);
            detailKaryawan.setKaryawan(newKaryawan);
            detailKaryawanRepo.save(detailKaryawan);
            return "redirect:/v1/view/karyawan/" + String.valueOf(newKaryawan.getId());
        } catch (Exception ex) {
            // log exception first,
            // then show error
            String errorMessage = ex.getMessage();
            // logger.error(errorMessage);
            model.addAttribute("errorMessage", errorMessage);

            //model.addAttribute("karyawan", karyawan);
            model.addAttribute("add", true);
            return "karyawan-edit";
        }
    }

    @GetMapping(value = {"/karyawan/{karyawanId}/edit"})
    public String showEditKaryawan(Model model, @PathVariable Long karyawanId) {
        Karyawan karyawan = null;
        try {
            karyawan = karyawanService.findById(karyawanId);

        } catch (Exception ex) {
            model.addAttribute("errorMessage", "Karyawan not found");
        }
        model.addAttribute("add", false);
        model.addAttribute("karyawan", karyawan);
        return "karyawan-edit";
    }

    @PostMapping(value = {"/karyawan/{karyawanId}/edit"})
    public String updateKaryawan(Model model,
                                @PathVariable Long karyawanId,
                                @ModelAttribute("karyawan") Karyawan karyawan) {
        try {
            karyawan.setId(karyawanId);
            karyawanService.update(karyawan);
            return "redirect:/v1/view/karyawan/" + String.valueOf(karyawan.getId());
        } catch (Exception ex) {
            // log exception first,
            // then show error
            String errorMessage = ex.getMessage();
            model.addAttribute("errorMessage", errorMessage);

            model.addAttribute("add", false);
            return "karyawan-edit";
        }
    }

    @GetMapping(value = {"/karyawan/{karyawanId}/delete"})
    public String showDeleteKaryawanById(
            Model model, @PathVariable Long karyawanId) {
        Karyawan karyawan = null;
        try {
            karyawan = karyawanService.findById(karyawanId);
        } catch (Exception ex) {
            model.addAttribute("errorMessage", "Karyawan not found");
        }
        model.addAttribute("allowDelete", true);
        model.addAttribute("karyawan", karyawan);
        return "karyawan";
    }

    @PostMapping(value = {"/karyawan/{karyawanId}/delete"})
    public String deleteKaryawanById(
            Model model, @PathVariable Long karyawanId) {
        try {
            karyawanService.delete(karyawanId);
            return "redirect:/v1/view/karyawan";
        } catch (Exception ex) {
            String errorMessage = ex.getMessage();
            // logger.error(errorMessage);
            model.addAttribute("errorMessage", errorMessage);
            return "karyawan";
        }
    }
}
