package com.binar.karyawan.controller;

import com.binar.karyawan.entity.Karyawan;
import com.binar.karyawan.entity.KaryawanTraining;
import com.binar.karyawan.service.interfaces.KaryawanTrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(value = "v1/karyawantraining")
public class KaryawanTrainingController {
    @Autowired
    KaryawanTrainingService karyawanTrainingService;

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody KaryawanTraining objModel) {
        Map obj = karyawanTrainingService.insert(objModel);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<Map> getAll(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "sort-by", required = false) String sortAttribute,
            @RequestParam(value = "sort-type", required = false) String sortType
    ) {
        page = (page == null) ? 0 : page;
        size = (size == null) ? 20 : size;
        sortAttribute = (sortAttribute == null) ? "" : sortAttribute;
        sortType = (sortType == null) ? "" : sortType;

        Map obj = karyawanTrainingService.getAll(page, size, sortAttribute, sortType);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id, @Valid @RequestBody KaryawanTraining objModel) {
        objModel.setId(id);

        Map obj = karyawanTrainingService.update(objModel);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else if ((int) obj.get("status") == 404){
            return new ResponseEntity<Map>(obj, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id) {
        Map obj = karyawanTrainingService.delete(id);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else if ((int) obj.get("status") == 404){
            return new ResponseEntity<Map>(obj, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
