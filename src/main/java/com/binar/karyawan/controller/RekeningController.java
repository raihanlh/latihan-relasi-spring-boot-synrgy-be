package com.binar.karyawan.controller;

import com.binar.karyawan.entity.Rekening;
import com.binar.karyawan.service.interfaces.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(value = "v1/rekening")
public class RekeningController {
    @Autowired
    RekeningService rekeningService;

    @PostMapping("/save/{karyawanId}")
    public ResponseEntity<Map> save(@Valid @RequestBody Rekening objModel, @PathVariable("karyawanId") Long karyawanId) {
        Map obj = rekeningService.insert(objModel, karyawanId);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id, @Valid @RequestBody Rekening objModel) {
        objModel.setId(id);
        Map obj = rekeningService.update(objModel);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else if ((int) obj.get("status") == 404){
            return new ResponseEntity<Map>(obj, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Map> getById(@PathVariable("id") Long id) {
        Map obj = rekeningService.getById(id);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else if ((int) obj.get("status") == 404){
            return new ResponseEntity<Map>(obj, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/rek-id/{id}")
    public ResponseEntity<Map> getByKaryawanId(@PathVariable("id") Long id) {
        Map obj = rekeningService.getByKaryawanId(id);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else if ((int) obj.get("status") == 404){
            return new ResponseEntity<Map>(obj, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id) {
        Map obj = rekeningService.delete(id);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else if ((int) obj.get("status") == 404){
            return new ResponseEntity<Map>(obj, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
