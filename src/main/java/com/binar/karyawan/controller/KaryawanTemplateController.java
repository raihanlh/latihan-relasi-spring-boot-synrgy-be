package com.binar.karyawan.controller;

import com.binar.karyawan.entity.Karyawan;
import com.binar.karyawan.service.interfaces.KaryawanTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@Controller
@RequestMapping("v1/rt/")
public class KaryawanTemplateController {
    @Autowired
    private KaryawanTemplateService service;

    @GetMapping("/list")
    @ResponseBody
    public ResponseEntity<Map> getList() {
        Map c = service.getAll();
        return new ResponseEntity<Map>(c, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Karyawan karyawan) {
        Map obj = service.insert(karyawan);

        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id, @Valid @RequestBody Karyawan karyawan) {
        karyawan.setId(id);
        Map obj = service.update(karyawan);

        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id) {
        Map obj = service.delete(id);

        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }
}
