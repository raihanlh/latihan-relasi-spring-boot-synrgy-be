package com.binar.karyawan.controller;

import com.binar.karyawan.entity.Karyawan;
import com.binar.karyawan.entity.Training;
import com.binar.karyawan.service.interfaces.KaryawanService;
import com.binar.karyawan.service.interfaces.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(value = "v1/training")
public class TrainingController {
    @Autowired
    TrainingService trainingService;

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Training objModel) {
        Map obj = trainingService.insert(objModel);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<Map> getAll(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "sort-by", required = false) String sortAttribute,
            @RequestParam(value = "sort-type", required = false) String sortType
    ) {
        page = (page == null) ? 0 : page;
        size = (size == null) ? 20 : size;
        sortAttribute = (sortAttribute == null) ? "" : sortAttribute;
        sortType = (sortType == null) ? "" : sortType;

        Map obj = trainingService.getAll(page, size, sortAttribute, sortType);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
