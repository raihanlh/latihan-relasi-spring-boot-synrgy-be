package com.binar.karyawan.repository;

import com.binar.karyawan.entity.Karyawan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface KaryawanRepo extends PagingAndSortingRepository<Karyawan, Long> {
    // JPQL
    @Query("SELECT k FROM Karyawan k WHERE k.id = :id")
    Karyawan getById(@Param("id") long id);

    @Query("SELECT k FROM Karyawan k WHERE deleted_at IS NOT NULL")
    List<Karyawan> getAll();

    // JPA
    Optional<Karyawan> findById(Long id);

    Page<Karyawan> findByNamaAndStatus(String nama, Integer status, Pageable pageable);

    Page<Karyawan> findAll(Pageable pageable);
}
