package com.binar.karyawan.repository;

import com.binar.karyawan.entity.Rekening;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RekeningRepo extends PagingAndSortingRepository<Rekening, Long> {
    @Query("SELECT r FROM Rekening r WHERE r.id = :id")
    Rekening getById(@Param("id") Long id);

    @Query("SELECT r FROM Rekening r WHERE r.karyawan.id = :idKaryawan")
    List<Rekening> getByKaryawanId(@Param("id") Long idKaryawan);

    @Query("SELECT r FROM Rekening r WHERE r.karyawan.id = :idKaryawan AND r.jenis = :jenis")
    Page<Rekening> getByJenisAndKaryawanId(@Param("jenis") String jenis, @Param("id") Long idKaryawan, Pageable pageable);
}
