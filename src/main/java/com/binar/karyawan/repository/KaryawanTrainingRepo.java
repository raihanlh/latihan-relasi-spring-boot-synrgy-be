package com.binar.karyawan.repository;

import com.binar.karyawan.entity.KaryawanTraining;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface KaryawanTrainingRepo extends PagingAndSortingRepository<KaryawanTraining, Long> {
    Page<KaryawanTraining> findAll(Pageable pageable);

    @Query("SELECT k FROM KaryawanTraining k WHERE k.id = :id")
    KaryawanTraining getById(@Param("id") long id);
}
