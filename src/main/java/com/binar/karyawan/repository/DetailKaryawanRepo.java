package com.binar.karyawan.repository;

import com.binar.karyawan.entity.DetailKaryawan;
import com.binar.karyawan.entity.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailKaryawanRepo extends JpaRepository<DetailKaryawan, Long> {
    @Query("SELECT d FROM DetailKaryawan d WHERE d.id = :id")
    DetailKaryawan getById(@Param("id") Long id);
}
