package com.binar.karyawan.repository;

import com.binar.karyawan.entity.Training;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainingRepo extends PagingAndSortingRepository<Training, Long> {
    @Query("SELECT t FROM Training t WHERE t.id = :id")
    Training getById(@Param("id") long id);

    Page<Training> findAll(Pageable pageable);
}
