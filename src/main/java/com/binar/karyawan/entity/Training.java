package com.binar.karyawan.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "training")
@Where(clause = "deleted_at is null")
public class Training extends DateProps implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "tema", length = 45, nullable = false)
    private String tema;

    @Column(name = "nama_pengajar", length = 30, nullable = false)
    private String namapengajar;

    @JsonIgnore
    @OneToMany(mappedBy = "training")
    List<KaryawanTraining> karyawantraining;
}
