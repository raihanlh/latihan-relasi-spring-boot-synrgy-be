package com.binar.karyawan.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "rekening")
@Where(clause = "deleted_at is null")
public class Rekening extends DateProps implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nama", length = 20, nullable = false)
    private String nama;

    @Column(name = "jenis", length = 20, nullable = false)
    private String jenis;

    @Column(name = "nomor", length = 20, nullable = false)
    private String nomor;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_karyawan", referencedColumnName = "id", nullable = false)
    private Karyawan karyawan;
}
