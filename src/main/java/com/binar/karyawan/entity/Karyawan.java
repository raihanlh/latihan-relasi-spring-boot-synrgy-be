package com.binar.karyawan.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "karyawan")
@Where(clause = "deleted_at is null") // Only fetch if deleted_at is null
public class Karyawan extends DateProps implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nama", nullable = false, length = 30)
    private String nama;

    @Column(name = "jenis_kelamin", nullable = false, length = 10)
    private String jeniskelamin;

    @Column(name = "tanggal_lahir", nullable = false)
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern="dd-MM-yyyy")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date tanggallahir;

    @Column(name = "alamat", nullable = false, columnDefinition = "TEXT")
    private String alamat;

    @Column(name = "status", nullable = false)
    private Integer status;

    @Column(name = "filename")
    private String filename;

    @OneToOne(mappedBy = "karyawan")
    DetailKaryawan detailkaryawan;

    @OneToMany(mappedBy = "karyawan")
    List<Rekening> rekening;

    @JsonIgnore
    @OneToMany(mappedBy = "karyawan")
    List<KaryawanTraining> karyawantraining;
}
