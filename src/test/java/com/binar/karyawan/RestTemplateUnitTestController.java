package com.binar.karyawan;

import com.binar.karyawan.entity.Karyawan;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RestTemplateUnitTestController {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void restTemplateSaveKaryawan() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "*/*");
        headers.set("Content-Type", "application/json");
        String bodyTesting = "{\n" +
            " \"nama\":\"Test Unit 2\",\n" +
            " \"jeniskelamin\":\"pria\",\n" +
            " \"tanggallahir\":\"11-05-1994\",\n" +
            " \"alamat\":\"Alamat test 2\",\n" +
            " \"status\":\"1\",\n" +
            " \"detailkaryawan\": {\n" +
            " \"nik\":\"12345244\",\n" +
            " \"npwp\":\"41234131\"\n" +
            " }\n" +
            "}";
        HttpEntity<String> entity = new HttpEntity<String>(bodyTesting, headers);
        ResponseEntity<String> exchange = restTemplate.exchange("http://localhost:9090/v1/karyawan/save", HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.OK, exchange.getStatusCode());
        System.out.println("response save barang="+exchange.getBody());
    }

    @Test
    public void restTemplateListKaryawan() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "*/*");
        headers.set("Content-Type", "application/json");
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> exchange = restTemplate
                .exchange("http://localhost:9090/v1/karyawan/all?page=0&size=5&sortAttribute=&sortType=",
                        HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, exchange.getStatusCode());
        System.out.println("response list karyawan="+exchange.getBody());
    }

    @Test
    public void restTemplateUpdateKaryawan() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "*/*");
        headers.set("Content-Type", "application/json");
        String bodyTesting = "{\n" +
                "    \"nama\": \"Test Update\",\n" +
                "    \"jeniskelamin\": \"pria\",\n" +
                "    \"tanggallahir\": \"11-05-1994\",\n" +
                "    \"alamat\": \"Jalan Konoha No 4\",\n" +
                "    \"status\": \"1\",\n" +
                "    \"detailkaryawan\": {\n" +
                "        \"nik\":1234456,\n" +
                "        \"npwp\":4142313\n" +
                "    }\n" +
                "}";
        HttpEntity<String> entity = new HttpEntity<String>(bodyTesting, headers);
        ResponseEntity<String> exchange = restTemplate.exchange("http://localhost:9090/v1/karyawan/update/2",
                HttpMethod.PUT, entity, String.class);
        assertEquals(HttpStatus.OK, exchange.getStatusCode());
        System.out.println("response update barang="+exchange.getBody());
    }

    @Test
    public void restTemplateDeleteKaryawan() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "*/*");
        headers.set("Content-Type", "application/json");
        String bodyTesting = "{\n" +
                " \"nama\":\"Test Unit 2\",\n" +
                " \"jeniskelamin\":\"pria\",\n" +
                " \"tanggallahir\":\"11-05-1994\",\n" +
                " \"alamat\":\"Alamat test 2\",\n" +
                " \"status\":\"1\",\n" +
                " \"detailkaryawan\": {\n" +
                " \"nik\":\"12345244\",\n" +
                " \"npwp\":\"41234131\"\n" +
                " }\n" +
                "}";
        HttpEntity<String> entity = new HttpEntity<String>(bodyTesting, headers);
        ResponseEntity<Map> inserted = restTemplate.exchange("http://localhost:9090/v1/karyawan/save",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<Map>(){});

        Map data = (Map) inserted.getBody().get("data");
        System.out.println(data.get("id"));

        ResponseEntity<String> exchange = restTemplate.exchange("http://localhost:9090/v1/karyawan/delete/" +
                        data.get("id"),
                HttpMethod.DELETE,
                null,
                String.class);

        assertEquals(HttpStatus.OK, exchange.getStatusCode());
        System.out.println("response delete barang="+exchange.getBody());
    }

    private <T> T mapFromJson(String json, Class<T> tClass) throws JsonParseException, JsonMappingException,
            IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, tClass);
    }
}
