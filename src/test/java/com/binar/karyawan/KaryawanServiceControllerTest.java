package com.binar.karyawan;

import com.binar.karyawan.entity.DetailKaryawan;
import com.binar.karyawan.entity.Karyawan;
import com.binar.karyawan.repository.KaryawanRepo;
import com.binar.karyawan.service.interfaces.KaryawanService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class KaryawanServiceControllerTest extends UnitTest {
    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Autowired
    private KaryawanRepo karyawanRepo;

    @Autowired
    private KaryawanService karyawanService;

    @Test
    public void getKaryawanList() throws Exception {
        String uri = "http://localhost:9090/v1/karyawan/all?page=0&size=5&sortAttribute=&sortType=";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .get(uri)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        System.out.println("Response=");
        System.out.println(content);
    }

    @Test
    public void insertKaryawan() throws Exception {
        String uri = "http://localhost:9090/v1/karyawan/save";
        DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

        Karyawan karyawan = new Karyawan();
        karyawan.setNama("Unit Test");
        karyawan.setJeniskelamin("pria");
        karyawan.setTanggallahir(dateFormatter.parse("11-05-1994"));
        karyawan.setAlamat("Alamat test");
        karyawan.setStatus(1);

        DetailKaryawan detailKaryawan = new DetailKaryawan();
        detailKaryawan.setNpwp("412341");
        detailKaryawan.setNik("412347");

        karyawan.setDetailkaryawan(detailKaryawan);

        MvcResult mvcResult= mvc.perform(MockMvcRequestBuilders
                .post(uri)
                .content(asJsonString(karyawan))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        System.out.println("Response=");
        System.out.println(content);
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void checkKelilingLingkaran() {
        final double PI = 3.14;
        double r = 10;
        double keliling = PI*2*r;

        assertEquals(62.8,keliling, 0.000001);
    }
}
